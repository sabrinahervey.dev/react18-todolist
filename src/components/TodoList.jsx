import PropTypes from "prop-types";
import TodoItem from "./TodoItem";

function TodoList(props) {
  const { todos } = props;
  //utilisation de la méthode Map sur le array todos pour créer une liste
  const listItem = todos.map((todos) => 
  //on attribue un key pour identifier les élèments
  <TodoItem key={todos.id} title={todos.title} completed={todos.completed}/>)

  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul>{listItem}</ul>
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
